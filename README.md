# Aisle-6593

## Image validation tools with edition

------------------

* Server : NodeJS
* API : SightEngine (https://sightengine.com/)
* Libraries : CropperJS (https://github.com/fengyuanchen/cropperjs), node-opencv (https://github.com/LorrisSaintGenez/node-opencv)