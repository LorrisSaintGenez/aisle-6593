const express = require('express');
const router = express.Router();
const fs = require('fs');
const buffer = require('buffer');
const sightengine = require('sightengine')("490037725", "nncHygT4RiNWzk7s9DrA");

const sightEngineFunctions = require('../public/javascripts/sightEngine.js');
const propertiesSightEngine = ['properties', 'face', 'faces', 'offensive', 'scam', 'face-attributes'];

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Upload your photo' });
});

router.post('/upload', function(req, res, next) {
    req.pipe(req.busboy);
    req.busboy.on('file', function (fieldname, file, filename) {
        console.log('Uploading: ' + filename);
        const filePath = './public/images/' + filename;
        const fstream = fs.createWriteStream(filePath);
        file.pipe(fstream);
        fstream.on('close', function () {
          sightengine.check(propertiesSightEngine).set_file(filePath).then(function(result) {
              sightEngineFunctions.resultAnalysis(result, '/images/' + filename, res);
          }).catch(function(err) {
              // handle the error
          });
        });
    });
});

router.post('/submit-edited-photo', function (req, res, next) {
  const imageData = req.body.imageData.replace(/^data:image\/png;base64,/, "");
  const filePath = './public' + req.body.imageUrl;
  fs.writeFile(filePath, imageData, 'base64', function (err) {
    if (err)
      console.log(err);
    else {
      sightengine.check(propertiesSightEngine).set_file(filePath).then(function (result) {
        sightEngineFunctions.resultAnalysis(result, req.body.imageUrl, res);
      }).catch(function (err) {
        // handle the error
      });
    }
  });
});

router.post('/force-submit', function (req, res, next) {
  res.render('succeded/success', { title: 'Success', imageUrl: req.body.imageUrl});
});

module.exports = router;
