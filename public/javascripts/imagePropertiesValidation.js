$(document).ready(function(){
  
  var _URL = window.URL || window.webkitURL;
  
  $('#file').change(function () {
    var file = $(this)[0].files[0];
    
    img = new Image();
    var imgwidth = 0;
    var imgheight = 0;
    var minWidth = 200;
    var minHeight = 200;
    
    $("#submit").prop('disabled', true);
    
    img.src = _URL.createObjectURL(file);
    img.onload = function() {
      imgwidth = this.width;
      imgheight = this.height;
      
      $("#width").text(imgwidth);
      $("#height").text(imgheight);
      if(imgwidth < minWidth || imgheight < minHeight){
        $("#response").text("Image size must be at least " + minWidth + "x" + minHeight + " pixels");
      } else {
        $("#submit").prop('disabled', false);
        $("#response").text("");
      }
    };
    img.onerror = function() {
      $("#response").text("Not a valid file");
    }
    
  });
});