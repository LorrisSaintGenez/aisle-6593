var cv = require('opencv');
var Promise = require('promise');

module.exports = {
  
  resultAnalysis: function(result, filename, res) {
    console.log(result);
    
    var high_rejection = false;
    var low_rejection = false;
    var edit_rejection = false;
    var invalid_content = [];
    
    if (result.face.multiple > 0.7 || result.faces.length > 1) {
      edit_rejection = true;
      invalid_content.push('There seems to be two or more faces in the photo');
    }
    if (result.scam.prob > 0.6) {
      low_rejection = true;
      invalid_content.push('The photo seems to be a scam');
    }
    /*if (result.nudity > 0.7) {
      high_rejection = true;
      invalid_content.push('Your photo seems to have nudity');
    }
    else {
      if (result.nudity > 0.3) {
        low_rejection = true;
        invalid_content.push('Your photo seems to have nudity');
      }
    }*/
    if (result.sharpness < 0.3) {
      high_rejection = true;
      invalid_content.push('Your photo seems to be blurry');
    }
    else {
      if (result.sharpness < 0.6) {
        low_rejection = true;
        invalid_content.push('The photo seems to be a bit blurry');
      }
    }
    if (result.offensive > 0.5) {
      edit_rejection = true;
      invalid_content.push('Your photo seems to contain offensive items');
    }
    if (result.faces.length === 1) {
      const faceBoundaries = result.faces[0];
      if (faceBoundaries.attributes.sunglasses > 0.6) {
        high_rejection = true;
        invalid_content.push('Your eyes are not clearly visible');
      }
      const minBoundary = 0.001;
      const maxBoundary = 0.999;
      if (faceBoundaries.x1 < minBoundary || faceBoundaries.x2 < minBoundary
        || faceBoundaries.y1 < minBoundary || faceBoundaries.y2 < minBoundary
        || faceBoundaries.x1 > maxBoundary || faceBoundaries.x2 > maxBoundary
        || faceBoundaries.y1 > maxBoundary || faceBoundaries.y2 > maxBoundary) {
        high_rejection = true;
        invalid_content.push('Your face is not entirely visible');
      }
    }
    else {
      if (result.faces.length === 0)
      {
        high_rejection = true;
        invalid_content.push('You need to be in the photo and looking/facing at the camera');
      }
    }
    if (this.checkGreyscale(result.colors.dominant)) {
      high_rejection = true;
      invalid_content.push('Your photo is a grayscale photo.');
    }
    
    var isUpperBodyVisible = this.checkShoulders(result.faces[0].features, filename);
    isUpperBodyVisible.then(function (value) {
      if (!value) {
        low_rejection = true;
        invalid_content.push('It seems your shoulders are not fully visible.');
      }
      
      if (high_rejection)
        res.render('rejected/rejected_high', { title: 'High Rejection', reasons: invalid_content });
      else {
        if (edit_rejection)
          res.render('rejected/rejected_edit', { title: 'Edition', imageUrl: filename, reasons: invalid_content});
        else {
          if (low_rejection)
            res.render('rejected/rejected_low', { title: 'Low Rejection', imageUrl: filename, reasons: invalid_content});
          else {
            res.render('succeded/success', { title: 'Success', imageUrl: filename});
          }
        }
      }
    });
  
  },
  checkShoulders: function(face, filename) {
    return new Promise(function(resolve, reject) {
      cv.readImage('public' + filename, function(err, im){
        im.detectObject(cv.UPPERBODY_CASCADE, {}, function(err, upperBodyElements){
          if (err)
            reject(err);
          else
            resolve(upperBodyElements.length > 0);
        });
      });
    })
  },
  // check if photo is a black&white photo
  // return true if it is
  checkGreyscale: function(color) {
    return (color.r === color.g && color.r === color.b);
  }
};